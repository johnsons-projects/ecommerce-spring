package com.johnsonz.order_service;

import com.johnsonz.order_service.dto.OrderRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.testcontainers.containers.MySQLContainer;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderServiceApplicationTests {

	@ServiceConnection
	static MySQLContainer mySQLContainer = new MySQLContainer("mysql:8.3.0");
	@LocalServerPort
	private Integer port;

	@BeforeEach
	void setup() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = port;
	}

	static {
		mySQLContainer.start();
	}

	@Test
	void shouldSubmitOrder() throws Exception {
		OrderRequest orderRequest = getOrderRequest();

		var responseBodyString = RestAssured.given().
				contentType(ContentType.JSON).
				body(orderRequest).
				when().
				post("/api/order").
				then().
				log().all().
				statusCode(201).
				extract().
				body().asString();

		assertThat(responseBodyString, Matchers.is("Order Placed Successfully!"));
	}

	private OrderRequest getOrderRequest() {
		return new OrderRequest(null, UUID.randomUUID().toString(), "iphone_15", BigDecimal.valueOf(1000), 1);
	}
}
