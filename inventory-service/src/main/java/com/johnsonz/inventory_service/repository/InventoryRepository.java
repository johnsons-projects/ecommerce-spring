package com.johnsonz.inventory_service.repository;

import com.johnsonz.inventory_service.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    Boolean existsBySkuCodeAndQuantityGreaterThanEqual(String skuCode, Integer quantity);
}
